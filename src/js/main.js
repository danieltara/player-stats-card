"use strict";

( function() {
	window.addEventListener( 'DOMContentLoaded', () => {
		// Make a cache of all card elements
		var card      = document.getElementById( 'card' ),
			dropdown  = document.getElementById( 'dropdown' ),
			image     = document.getElementById( 'image' ),
			badge     = document.getElementById( 'badge' ),
			club      = document.getElementById( 'club' ),
			givenName = document.getElementById( 'given-name' ),
			position  = document.getElementById( 'position' ),
			statsList = document.getElementById( 'stats' )

		// Create a map of all positions
		var positions = {
				D: 'Defender',
				M: 'Midfielder',
				F: 'Forward',
			}

		// Create a map of all stats
		var stats = {
			goals:         'Goals',
			losses:        'Losses',
			wins:          'Wins',
			draws:         'Draws',
			fwd_pass:      'Forward passes',
			goal_assist:   'Goal assists',
			appearances:   'Appearances',
			mins_played:   'Minutes played',
			backward_pass: 'Backward passes',
		}

		const App = async () => {
			fetch( './data/player-stats.json' )
				.then( response => response.json() )
				.then( response => {
					// Remove 'Please wait...' option
					dropdown.textContent = ''

					/**
					 * Select a player option
					 */
					var option = document.createElement( 'option' )

					option.value     = -1
					option.innerHTML = 'Select a player&#8230;'
					option.disabled  = true
					option.selected  = true

					dropdown.appendChild( option )

					if ( response.players && response.players.length ) {
						/**
						 * Add players to dropdown
						 */
						response.players.forEach( ( data, index ) => {
							var option = document.createElement( 'option' )

							option.value       = index
							option.textContent = ( ( data.player.name.first || '' ) + ' ' + ( data.player.name.last || '' ) )

							dropdown.appendChild( option )
						} )

						/**
						 * Re-render card when selected player changes
						 */
						dropdown.addEventListener( 'change', function() {
							if ( response.players[ this.value ] ) {
								var data = response.players[ this.value ]

								/**
								 * Player info
								 */
								if ( data.player ) {
									/**
									 * Team badge
									 */
									image.src = data.player.id ? './assets/p' + data.player.id + '.png' : './assets/player-placeholder.png'

									badge.className = ''
									badge.classList.add( 'player-stats-card__club-badge' )
									badge.classList.add( 'player-stats-card__club-badge--' + data.player.currentTeam.id )

									club.textContent = data.player.currentTeam.name || ''

									// Player name
									givenName.textContent = ( ( data.player.name.first || '' ) + ' ' + ( data.player.name.last || '' ) )

									// Player position
									position.textContent = positions[ data.player.info.position ] || ''
								}

								/**
								 * Build player stats list
								 */
								statsList.textContent = ''

								data.stats.forEach( stat => {
									// Create new elements
									var listItem  = document.createElement( 'li' ),
										itemTitle = document.createElement( 'span' ),
										itemValue = document.createElement( 'strong' )

									// Stat title
									itemTitle.classList.add( 'player-stats-card__stat-title' )
									itemTitle.textContent = stats[ stat.name ] || ''

									// Stat value
									itemValue.classList.add( 'player-stats-card__stat-value' )
									itemValue.textContent = stat.value || ''

									// Build stat tree
									listItem.classList.add( 'player-stats-card__stat' )
									listItem.appendChild( itemTitle )
									listItem.appendChild( itemValue )

									// Add stat to list
									statsList.appendChild( listItem )
								} )
							}
						} )

						// Enable dropdown
						dropdown.disabled = false
					/**
					 * Edge case: no players available
					 */
					} else {
						var option = document.createElement( 'option' )

						option.textContent =  'No players available'

						dropdown.appendChild( option )
					}

					// Remove loading class from card element.
					card.classList.remove( 'player-stats-card--loading' )
				} )
		}

		// Initialise
		App()
	} )
} )()
