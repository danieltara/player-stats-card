const path    = require( 'path' );
const minicss = require( 'mini-css-extract-plugin' );

module.exports = {
	mode: 'development',
	resolve: {
		fallback: {
			'path': require.resolve( 'path-browserify' ),
		},
	},
	entry: {
		main:  path.resolve( 'src', 'js/main.js' ),
		style: path.resolve( 'src', 'sass/style.sass' ),
	},
	output: {
		path:     path.resolve( __dirname, 'dist' ),
		filename: 'js/[name].js',
	},
	plugins: [
		new minicss({
			filename: 'css/[name].css',
		})
	],
	module: {
		rules: [
			{
				test:    /\.sass$/,
				include: path.resolve( __dirname, 'src/sass' ),
				use:     [
					minicss.loader,
					'css-loader',
					'sass-loader',
				],
			},
			{
				test:    /\.js$/,
				include: path.resolve( __dirname, 'src/js' ),
				use:     'babel-loader',
			},
			{
				test:    /\.png$/,
				include: path.resolve( __dirname, 'src/images' ),
				use: [{
					loader:   'file-loader',
					options:  {
						name: '[name].[ext]',
						outputPath: 'assets/',
						publicPath: '../assets/',
					},
				}],
			},
		],
	},
}
