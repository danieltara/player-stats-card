Player Stats Card
=================

Requirements
------------

The project requires npm or Yarn to be installed locally. You can safely skip this step if either of these are already installed on your system. This readme covers installation on macOS.

* Install Homebrew:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

* Install Yarn:

```bash
brew install yarn --without-node
```

* Install Node which contains npm:

```bash
brew install node
```

Getting Started
---------------

These steps are required after every fresh installation of the project, otherwise your static assets will not compile.

* Navigate to the local repository location:

```bash
cd /path/to/local/player-stats-card
```

* Install all required dependencies:

```bash
yarn install
```

Editing Files
-------------

* To have a watch process active when editing SASS or JS files:

```bash
yarn watch
```

* To compile scripts for production:

```bash
yarn build
```
